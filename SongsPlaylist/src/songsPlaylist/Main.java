package songsPlaylist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {


	public static void main(String[] args) {
		// ArrayList<Song> Playlist;

		Song song1 = new Song("Two Feet - Lost The Game", 171);
		Song song2 = new Song("Alec Benjamin - Outrunning Karma", 188);
		Song song3 = new Song("X Ambassadors - Renegades", 193);
		Song song4 = new Song("The Police - Roxanne", 193);
		Song song5 = new Song("Queen - Don't Stop Me Now", 217);
		Song song6 = new Song("Sam Cooke - Wonderful World", 129);
		Song song7 = new Song("Roy Orbison - Oh, Pretty Woman", 177);
		Song song8 = new Song("The Chainsmokers - Sick Boy", 218);
		Song song9 = new Song("Vicetone - Nevada", 208);

		List<Album> AlbumList = new ArrayList<>();
		Album album1 = new Album();
		Album album2 = new Album();
		AlbumList.add(album1);
		AlbumList.add(album2);
		album1.SongsList.add(song1);
		album1.SongsList.add(song2);
		album1.SongsList.add(song3);
		album1.SongsList.add(song4);
		album2.SongsList.add(song5);
		album2.SongsList.add(song6);
		album2.SongsList.add(song7);
		album2.SongsList.add(song8);
		album2.SongsList.add(song9);
		Integer s = 0;

		List<Song> Playlist = new ArrayList<>();
		Playlist.add(album1.getSong(s));
		Playlist.add(album2.getSong(s));
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int choice = 1;
		int index = 0;
		while (choice != 0) {
			System.out.println("0.Quit");
			System.out.println("1.Play");
			System.out.println("2.Skip");
			System.out.println("3.Back");
			System.out.println("4.Search for a song");
			System.out.println("5.Remove current song");
			
			System.out.println("Your choice=");
			try {
				try {
					choice = Integer.parseInt(read.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid Format!");
			}

			String CurentSongName ;
			switch (choice) {
			case 0:
				System.exit(0);
				break;
			case 1:
				if(Playlist.size()==0) {
					System.out.println("The playlist is empty! Please add a song into the playlist");
					break;
				}
				index=0;
				CurentSongName = Playlist.get(0).getTitle();
				System.out.println("Now playing:" + CurentSongName);
				break;
			case 2:
				if(index==Playlist.size()-1) {
					System.out.println("You have reached the end of the playlist");
					break;
					}
				index++;
				CurentSongName = Playlist.get(index).getTitle();
				System.out.println("Now playing:" + CurentSongName);
				break;
			case 3:
				if(index==0) {
					System.out.println("You have reached the start of the playlist");
					break;
					}
				index--;
				CurentSongName = Playlist.get(index).getTitle();
				System.out.println("Now playing:" + CurentSongName);
				break;
			case 4:
				System.out.println("Please enter the name of the song you want to search");
				try {
					String search = read.readLine();
					Main.search(search, AlbumList, Playlist);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
			case 5:
				Playlist.remove(index);
				System.out.println("The song has been succesfully removed from the playlist");
				
			}
		}

	}
	public static void search(String name, List<Album> albumList, List<Song>Playlist) {
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		int choice=0;
		int found=0;
		//System.out.println("intra");
		for (Album album : albumList) {
			//List songsList = album.getSongsList();
			for( Song song: album.SongsList) {
				if(song.getTitle().contains(name)) {
					found=1;
					System.out.println("The song "+song.getTitle()+ " has been found, do you want to add it to playlist?");
					System.out.println("1.Yes");
					System.out.println("2.No");
					try {
						 choice = Integer.parseInt(read.readLine());
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(choice==1)
						Playlist.add(song);
				break;
				}
			
		}
		}if(found==0) {
			System.out.println("The song "+ name+ " has not been found! Please try again");
		}
		}
	}

