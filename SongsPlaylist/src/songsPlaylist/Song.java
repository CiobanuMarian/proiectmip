package songsPlaylist;

public class Song {
 public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public int getDuration() {
		return Duration;
	}
	public Song(String title, int duration) {
		super();
		Title = title;
		Duration = duration;
	}
	public void setDuration(int duration) {
		Duration = duration;
	}
String Title;
 int Duration;
 
}
