package songsPlaylist;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class MusicController {
	
	
	private List<Song> Playlist = new ArrayList<>();
	@FXML
    private Text CurrentSongPlay;
    
    private int index;
   // @FXML
    //private List<Song> Playlist = new ArrayList<>();
    @FXML
    private TextField songToSearch;
    @FXML
    private String CurentSongName ;
    @FXML
    void initialize(){
    	Song song1 = new Song("Two Feet - Lost The Game", 171);
		Song song2 = new Song("Alec Benjamin - Outrunning Karma", 188);
		Song song3 = new Song("X Ambassadors - Renegades", 193);
		Song song4 = new Song("The Police - Roxanne", 193);
		Song song5 = new Song("Queen - Don't Stop Me Now", 217);
		Song song6 = new Song("Sam Cooke - Wonderful World", 129);
		Song song7 = new Song("Roy Orbison - Oh, Pretty Woman", 177);
		Song song8 = new Song("The Chainsmokers - Sick Boy", 218);
		Song song9 = new Song("Vicetone - Nevada", 208);

		List<Album> AlbumList = new ArrayList<>();
		Album album1 = new Album();
		Album album2 = new Album();
		AlbumList.add(album1);
		AlbumList.add(album2);
		album1.SongsList.add(song1);
		album1.SongsList.add(song2);
		album1.SongsList.add(song3);
		album1.SongsList.add(song4);
		album2.SongsList.add(song5);
		album2.SongsList.add(song6);
		album2.SongsList.add(song7);
		album2.SongsList.add(song8);
		album2.SongsList.add(song9);
		Integer s = 0;

//		List<Song> Playlist = new ArrayList<>();
		Playlist.add(album1.getSong(s));
		Playlist.add(album2.getSong(s));
    }
    @FXML
    void playButton(MouseEvent event) {
    	if(Playlist.size()==0) {
			CurrentSongPlay.setText("The playlist is empty! Please add a song into the playlist");
		}
		index=0;
		CurentSongName = Playlist.get(0).getTitle();
		System.out.println("Now playing:" + CurentSongName);
    }

    @FXML
    void addPlaylist(MouseEvent event) {

    }

    @FXML
    void backButton(MouseEvent event) {

    }

    @FXML
    void searchSong(MouseEvent event) {

    }

    @FXML
    void skipButton(MouseEvent event) {

    }

}
