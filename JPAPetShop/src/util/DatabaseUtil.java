package util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

/**
 *This class will help us to handle the database
 */
public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	/**
	 * Initializes the entityManager and entityManagerFactory
	 * */
	public void setUP() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("JPAPetShop");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Starts the data transaction
	 * */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * The function inserts a new animal in the database
	 * @param animal is the new animal you want to insert in the database
	 * */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	public void savePersonal(Personalmedical pers) {
		entityManager.persist(pers);
	}
	
	public void saveAppointment(Programare prog) {
		entityManager.persist(prog);
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
			entityManager.close();
	}
	
	public List<Animal> animalList(){
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a From Animal a", Animal.class).getResultList();
		return animalList;
	}
	
	public List <Programare> progaramreLista(){
		
		List<Programare> programareList = (List<Programare>)entityManager.createQuery("SELECT a From Programare a", Programare.class).getResultList();
		return programareList;
	}
	
	public void printAllAnimalsFromDB() {
		//Date date = new Date();
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		for (Animal animal : results) {
				//System.out.println("Animal id :"+animal.getIdAnimal()+" name: "+animal.getName() +" owner: "+animal.getOwner()+" weight: "+ animal.getWeight());
						//+" birthdate: "+animal.getBirthdate()+" age: "+ (date.getTime() - animal.getBirthdate().getTime())/86400000/365);
		}
	}
	
	public void printAllDoctorsFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.personal", Personalmedical.class).getResultList();
		for (Personalmedical pers : results) {
				System.out.println("Personal id :"+ pers.getIdpersonal()+" name: "+pers.getNume());
		}
	}

	public void printAllAppointmentsFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare", Programare.class).getResultList();
		for (Programare prog : results) {
				System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+prog.getIdanimal()+" personal: "+prog.getIdpersonal()+" time: "+prog.getDate());
		}
	}
	
	public final Animal animalFind(int index) {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal where idAnimal ="+index, Animal.class).getResultList();
		return results.get(0);
	}
	
	public void updateAnimal(int index, String newName) {
		  Animal anim= (Animal)entityManager.find(Animal.class ,index);
		  anim.setName(newName);
		 // entityManager.getTransaction().commit();
	}

	public void deleteAnimal(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idanimal = "+index, Programare.class).getResultList();
		for (Programare prog : results) {
			entityManager.remove(prog);
		}
		  Animal anim= (Animal)entityManager.find(Animal.class ,index);
		  entityManager.remove(anim);
		 // entityManager.getTransaction().commit();
	}
	
	public final Personalmedical doctorFind(int index) {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.personalmedical where idpersonal ="+index, Personalmedical.class).getResultList();
		return results.get(0);
	}
	
	public void updateDoctor(int index, String newName) {
		Personalmedical pers= (Personalmedical)entityManager.find(Personalmedical.class ,index);
		  pers.setNume(newName);
		 // entityManager.getTransaction().commit();
	}
	
	public void deleteDoctor(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idpersonal = "+index, Programare.class).getResultList();
		for (Programare prog : results) {
			entityManager.remove(prog);
		}
		Personalmedical pers= (Personalmedical)entityManager.find(Personalmedical.class ,index);
		  entityManager.remove(pers);
		 // entityManager.getTransaction().commit();
	}
	
	public final Programare appointmentFind(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idprogramare ="+index,Programare.class).getResultList();
		return results.get(0);
	}
	
	public void deleteAppointment(int index) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  entityManager.remove(prog);
		 // entityManager.getTransaction().commit();
	}
	

	public void updateAppointmentAnimal(int index, int animalIndex) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setIdanimal(animalIndex);
		 // entityManager.getTransaction().commit();
	}
	
	public void updateAppointmentDoctor(int index, int doctorIndex) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setIdpersonal(doctorIndex);;
		//  entityManager.getTransaction().commit();
	}
	
	public void updateAppointmentDate(int index, Date date) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setDate(date);
		//  entityManager.getTransaction().commit();
	}
	
	public void sortPrint() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare order by date", Programare.class).getResultList();
		for (Programare prog : results) {
				System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+ this.animalFind(prog.getIdanimal()).getName()+" personal: "+this.doctorFind(prog.getIdpersonal()).getNume()+" time: "+prog.getDate());
		}
	}
	
}
