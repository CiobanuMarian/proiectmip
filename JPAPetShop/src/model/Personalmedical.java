package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personal database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonalMedical;

	private String NumePersonal;

	public Personalmedical() {
	}

	public int getIdpersonal() {
		return this.idPersonalMedical;
	}

	public void setIdpersonal(int idpersonal) {
		this.idPersonalMedical = idpersonal;
	}

	public String getNume() {
		return this.NumePersonal;
	}

	public void setNume(String name) {
		this.NumePersonal = name;
	}

}