package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private int idanimals;

	private int idpersonal;
	
	private String tipProgramare;

	public String getTipProgramare() {
		return tipProgramare;
	}

	public void setTipProgramare(String tipProgramare) {
		this.tipProgramare = tipProgramare;
	}

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdanimal() {
		return this.idanimals;
	}

	public void setIdanimal(int idanimal) {
		this.idanimals = idanimal;
	}

	public int getIdpersonal() {
		return this.idpersonal;
	}

	public void setIdpersonal(int idpersonal) {
		this.idpersonal = idpersonal;
	}

	
}