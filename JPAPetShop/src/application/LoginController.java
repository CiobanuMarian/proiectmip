package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sun.security.util.Password;

public class LoginController {
	@FXML
    private Pane rootPane;
	@FXML
	private ImageView loginImage;
    @FXML
    private PasswordField password;
    @FXML
    private Button logButton;
    @FXML
    private TextField usernameOrEmail;

    @FXML
    void CheckAccount(ActionEvent event) {
    	//FXMLLoader loader= new FXMLLoader(getClass().getResource("Sample.fxml"));
    	//Stage primaryStage = (Stage) logButton.getScene().getWindow();
    	//Scene scene = new Scene(loader.getRoot());
    	boolean check=false;
    	try {
			Socket socket = new Socket("localhost", 5000);
			BufferedReader echoes  = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(),true);
			String echoStrng;
			String username;
			String Password;
			Scanner sc =new Scanner(System.in);
			String response;
				username= usernameOrEmail.getText();
				Password= password.getText();
				stringToEcho.println(username);
				stringToEcho.println(Password);
				String checking;
				checking=echoes.readLine();
				if(checking != null) {
					if(checking.equals("true")) {
						 check = true;
					}
				}
				else check=false;


		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	if(check) {
    	Stage primaryStage = (Stage) logButton.getScene().getWindow();
    		 try {
    			 BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("Sample.fxml"));
    			 Scene scene = new Scene(root, 600, 500);
    			 primaryStage.setScene(scene);
    			 primaryStage.show();
    			 primaryStage.setTitle("Appoiments");
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    			
    		}else {Alert alert = new Alert(AlertType.INFORMATION);
    		alert.setTitle("Information Dialog");
    		alert.setHeaderText(null);
    		alert.setContentText("The Username and/or password is incorrect");

    		alert.showAndWait();}
    			
    	
    
    }
    @FXML
	void initialize() {
  
    Image img=new Image("file:image.png");
	loginImage.setImage(img);
	loginImage.setFitWidth(200);
	loginImage.setFitHeight(200);

	}

}
