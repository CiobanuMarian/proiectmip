package application;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
	 try {
		 Pane root = (Pane) FXMLLoader.load(getClass().getResource("LoginWindow.fxml"));
		 Scene scene = new Scene(root, 347, 407);
		 primaryStage.setScene(scene);
		 primaryStage.show();
		 primaryStage.setTitle("Appoiments");
	} catch (Exception e) {
		e.printStackTrace();
	}
		
	}

	public static void main (String[] args) {
		launch(args);
	}
 
}
