package application;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.time.Year;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import application.LongRunningTask;
import application.OnCompleteListener;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class SampleController {

	DatabaseUtil db;

	int currentId ;
	@FXML
	private ListView<String> appoimentsView;
	@FXML
	private TextField idName;
    @FXML
    private TextField idOwner;
	@FXML
	private TextField idAge;
	@FXML
	private TextField idWeight;
    @FXML
    private ImageView imageView;
    @FXML
    private TextField idRace;
    @FXML
    private Text idType;
    @FXML
    private Pane rootPane;
	

	@FXML
	void onSelectApp(MouseEvent event) {

		String textId = appoimentsView.getSelectionModel().getSelectedItem();
		textId = textId.substring(10);
		int id = Integer.parseInt(textId);
		currentId = id;
		Programare programare = db.appointmentFind(id);
		Animal animal = db.animalFind(programare.getIdanimal());
		// System.out.println(animal.getName());
		idName.setText(animal.getName());
		Date currnetDate = new Date();
		
		//float age = calculateAge(LocalDate.of(animal.getBirthdate().getYear(), animal.getBirthdate().getMonth(), animal.getBirthdate().getDay()),
				//LocalDate.of(currnetDate.getYear(), currnetDate.getMonth(), currnetDate.getDay()));
		float age= ageCalc(LocalDate.of(animal.getBirthdate().getYear(), animal.getBirthdate().getMonth(), animal.getBirthdate().getDay()));
		String textAge = age + "";
		idAge.setText(textAge);
		System.out.println(animal.getBirthdate().getYear());
		idOwner.setText(animal.getOwner());
		String weight= animal.getWeight() + "";
		idWeight.setText(weight);
		String source="file:dog"+animal.getIdAnimal()+".jpg";
		Image img=new Image(source);
		imageView.setImage(img);
		idRace.setText(animal.getRace());
		idType.setText(programare.getTipProgramare());
	}

	@FXML
	void OnDonePress(ActionEvent event) {
		LongRunningTask longRunningTask = new LongRunningTask();
		longRunningTask.setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void onComplete() {
				System.out.println("The appoiment has been deleted");
			}
		});
		System.out.println("Deleting the selected appoiment");
		db.deleteAppointment(currentId);
		populateAppoimentsView();
		longRunningTask.run();
	}
	@FXML
	void OnExitPress(ActionEvent event)  {
		Platform.exit();
	    }
	public void populateAppoimentsView() {

		List<Programare> programareDBList = (List<Programare>) db.progaramreLista();
		ObservableList<String> programareLista = getprogramareId(programareDBList);
		appoimentsView.setItems(programareLista);
		appoimentsView.refresh();
		// db.closeEntityManager();
	}

	public ObservableList<String> getprogramareId(List<Programare> programari) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Programare a : programari) {
			names.add("Appoiment " + a.getIdprogramare());
		}
		return names;
	}

	

	@FXML
	void initialize() {
		setDBConnection();
		populateAppoimentsView();

	}

	private void setDBConnection() {
		db = new DatabaseUtil();
		try {
			db.setUP();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ERR");
		}
		db.startTransaction();
	}
	
	public static float calculateAge(LocalDate birthDate, LocalDate currentDate) {
		if ((birthDate != null) && (currentDate != null)) {
			return Period.between(birthDate, currentDate).getYears();
		} else {
			return 0;
		}
	}
	public static float ageCalc(LocalDate start) {
		//LocalDate start = LocalDate.of(1996, 2, 29);
		LocalDate end =LocalDate.now(); // use for age-calculation: LocalDate.now()
		System.out.println(start);
		long years = ChronoUnit.YEARS.between(start, end);
		return years;
	}
}
